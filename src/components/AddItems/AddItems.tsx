import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

import { ErrorMessage, FormField } from "..";
import ListContext, { ContextType, SetContextType } from "../App/App.types";

const AddItems = () => {
    const navigate = useNavigate();

    const [name, setName] = useState<string>("");
    const [id, setId] = useState<string>("");
    const [error, setError] = useState<string>("");

    const handleClick = (curList : ContextType, setCurList: SetContextType): void => {
        if (!name || !id) {
            if (!name && !id) setError("empty");
            else if (!name) setError("name")
            else setError("id");
        } else {
            setCurList([...curList, { name, id }]);
            navigate("/localhost/list");
        }
    };

    return (
        <ListContext.Consumer>
            {({ context, setContext }) => (
            <div>
                <FormField label="name" value={name} setValue={setName} />
                <FormField label="ID" value={id} setValue={setId} />
                <button onClick={(): void => handleClick(context, setContext)}>Save</button>
                <button>
                    <Link style={{ textDecoration: "none" }} to="/localhost/list">Cancel</Link>
                </button>
                {error && <ErrorMessage error={error} />}
            </div>
            )}
        </ListContext.Consumer>
    );
};

export default AddItems;
