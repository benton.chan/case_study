import React from "react";
import { Link } from "react-router-dom";

import ListContext from "../App/App.types";

import "./ItemList.css";

const ItemList = () => (
    <ListContext.Consumer>
        {({ context }) => (
            <div>
                <table className="ItemList__table">
                    <tr>
                        <th>Name</th>
                        <th>ID</th>
                    </tr>
                    <tbody>
                        {context.map(({ name, id }, index) => (
                            <tr key={`Item-${index}`}>
                                <td>{name}</td>
                                <td>{id}</td>
                            </tr>
                        ))}
                    </tbody>

                </table>
                <button>
                    <Link style={{ textDecoration: "none" }} to="/localhost/add">Add Items</Link>
                </button>
            </div>
        )}
    </ListContext.Consumer>
);

export default ItemList;
