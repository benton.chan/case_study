import React from "react";

const getErrorMessage = (error: string) => error === "empty" ? "Form fields are empty" : `${error} is empty`;
type errorType = string;

const ErrorMessage = ({ error }: { error: errorType }) => (
    <div className="ErrorMessage">{getErrorMessage(error)}</div>
);

export default ErrorMessage;
