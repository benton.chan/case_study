import React, { useState } from "react";
import { Outlet } from "react-router-dom";

import { Sidebar } from "..";

import dummyData from "./dummyData";
import ListContext, { ContextType } from "./App.types";
import "./App.css";

const App = () => {
    const [context, setContext] = useState<ContextType>([...dummyData]);

    return (
      <ListContext.Provider value={{ context, setContext }}>
          <div className="App">
              <Sidebar />
              <Outlet />
          </div>
      </ListContext.Provider>
  );
};

export default App;
