import { createContext } from "react";

export type ListItemType = {
    name: string
    id: string
};

export type ContextType = ListItemType[];
export type SetContextType = Function;

export type ListContextType = {
    context: ContextType,
    setContext: SetContextType
}

const ListContextState = {
    context: [],
    setContext: () => {}
};

const ListContext = createContext<ListContextType>(ListContextState);

export default ListContext;