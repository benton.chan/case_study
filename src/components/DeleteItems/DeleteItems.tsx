import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import ListContext, { ContextType, SetContextType } from "../App/App.types";

import "./DeleteItems.css";

const DeleteItems = () => {
    const navigate = useNavigate();
    const [isCheckAll, setIsCheckAll] = useState<boolean>(false);
    const [isChecked, setIsChecked] = useState<number[]>([]);

    const handleSelectAll = (length: number): void => {
        setIsCheckAll(!isCheckAll);
        if (isCheckAll) {
            setIsChecked([]);
        } else {
            setIsChecked(Array.from({ length }, (_, i) => i));
        }
    };

    const handleSelectOne = (checked: boolean, index: number) => {
        checked
            ? setIsChecked([...isChecked, index])
            : setIsChecked(isChecked.filter(item => item !== index))
    };

    const handleDelete = (context: ContextType, setContext: SetContextType) => {
        // added new line to filter fn for readability
        setContext(context.filter(
            (_, i) => !isChecked.includes(i)
        ));
        navigate("/localhost/list")
    };

    return (
        <ListContext.Consumer>
            {({ context, setContext }) => (
                <div>
                    <table className="DeleteItems__table">
                        <tr>
                            <th>Name</th>
                            <th>ID</th>
                            <th>
                                <input
                                    type="checkbox"
                                    name="selectAll"
                                    id="selectAll"
                                    checked={isCheckAll}
                                    onChange={() => handleSelectAll(context.length)}
                                />
                            </th>
                        </tr>
                        <tbody>
                        {context.map(({ name, id }, index) => (
                            <tr key={`DeleteItem-${index}`}>
                                <td>{name}</td>
                                <td>{id}</td>
                                <td>
                                    <input
                                        type="checkbox"
                                        name={name}
                                        id={`deleteItem-${index}`}
                                        checked={isChecked.includes(index)}
                                        onChange={({ target: { checked }}) => handleSelectOne(checked, index)}
                                    />
                                </td>
                            </tr>
                        ))}
                        </tbody>

                    </table>
                    <button onClick={() => handleDelete(context, setContext)}>Delete</button>
                </div>
            )}
        </ListContext.Consumer>
    );
};

export default DeleteItems;
