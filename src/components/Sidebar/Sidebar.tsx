import React from "react";
import { Link } from "react-router-dom";

import "./Sidebar.css";

const menuLinks = [{
    item: "Item List",
    route: "/localhost/list"
}, {
    item: "Add Items",
    route: "/localhost/add"
}, {
    item: "Delete Items",
    route: "/localhost/delete"
}];

const Sidebar = () => (
    <nav className="Sidebar">
        {menuLinks.map(({ item, route }, i) => (
            <Link className="Sidebar__menuLink" to={route} key={i}>{item}</Link>
        ))}
    </nav>
);

export default Sidebar;
