export interface FormFieldProps {
    label: string
    value: string
    setValue: (value: string) => void
}