import React from "react";

import { FormFieldProps } from "./FormField.types";

// Note: htmlFor/id is overly simplified, I typically pass a string prop separately, if necessary.
const FormField = ({ label, value, setValue } : FormFieldProps) => {

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setValue(e.target.value);
    }

    return (
        <div>
            <label htmlFor={label}>
                {label}
                <span style={{ color: "red" }}>*</span>
            </label>
            <input id={label} value={value} onChange={handleChange} />
        </div>
    );
};

export default FormField;
