import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { AddItems, App, DeleteItems, ItemList } from './components';

import './index.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
    <BrowserRouter>
          <Routes>
                <Route path="/" element={<App />}>
                      <Route path="/localhost/list" element={<ItemList />} />
                      <Route path="/localhost/add" element={<AddItems />} />
                      <Route path="/localhost/delete" element={<DeleteItems />} />
                </Route>
          </Routes>
      </BrowserRouter>
);