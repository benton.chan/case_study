Installation (requires npm):
- `npm i`

To start:
- `npm start`

========

Case Study:
1. Page with Left and Right section - Left side area will be sidebar with Menu items [Items List, Add Items, Delete Items], Right section to have the view part.
2. Clicking on left side menu item - route call is made and related component will show up in right view section. Routes to be like [ex: localhost/list, localhost/add, localhost/delete]
3. [localhost/list] - Display empty table with Add button –
4. Clicking on add button route happens to localhost/add and a Form is displayed with 2 input box labelled Name & ID fields. Page to have save and cancel buttons
5. Few validations on adding items - preferably empty checks and required fields.
6. On click of save button - Use local storage or state management to store and retrieve items in list page
7. On Click of cancel button - redirect to list page without adding data
8. On delete menu click - redirect to localhost/delete - show all stored items with checkbox. Provide option for "select all"  in the table so multiselect/select all will help us in deleting multiple items.
8. After successful delete - redirect back to [localhost/list] page with updated/refreshed items.